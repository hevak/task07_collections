package com.epam.edu.online;

import com.epam.edu.online.binarytree.ViewTree;
import com.epam.edu.online.enumconsole.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        // custom binary tree
        new ViewTree().runTreeDemo();
        // console view
        new ConsoleView().show();

    }
}
