package com.epam.edu.online.enumconsole.view;

@FunctionalInterface
public interface Printable {

  void print();
}
