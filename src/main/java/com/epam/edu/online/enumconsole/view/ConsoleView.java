package com.epam.edu.online.enumconsole.view;



import com.epam.edu.online.enumconsole.model.FunctionName;
import com.epam.edu.online.enumconsole.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This is class for view
 */
public class ConsoleView {
    private final static Logger LOGGER = LogManager.getLogger(ConsoleView.class);

    private ControllerImpl controller;
    private Map<FunctionName, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public ConsoleView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();


        menu.put(FunctionName.ALL, FunctionName.ALL+"\t\t"+FunctionName.ALL.getDescription());
        menu.put(FunctionName.ADD, FunctionName.ADD+"\t\t"+FunctionName.ADD.getDescription());
        menu.put(FunctionName.REMOVE, FunctionName.REMOVE+"\t"+FunctionName.REMOVE.getDescription());
        menu.put(FunctionName.Q, FunctionName.Q+"\t\t"+FunctionName.Q.getDescription());

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put(FunctionName.ALL.name(), this::all);
        methodsMenu.put(FunctionName.ADD.name(), this::add);
        methodsMenu.put(FunctionName.REMOVE.name(), this::remove);
        methodsMenu.put(FunctionName.HELP.name(), this::help);
    }

    private void remove() {
        LOGGER.info("remove");
    }

    private void add() {
        LOGGER.info("add");

    }

    private void all() {
        LOGGER.info("all");
    }

    private void help() {
        LOGGER.info("all");
        outputMenu();
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * method for showing message to write a command
     */
    public void show() {
        String keyMenu;
        do {
            System.out.print("\nPlease, enter command: ");
            keyMenu = input.nextLine().toUpperCase();

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                LOGGER.error("Wrong command, try one more time or write command `help`");
            }
        } while (!keyMenu.equals("q"));
    }
}
