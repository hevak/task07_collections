package com.epam.edu.online.enumconsole.model;

public enum FunctionName {
    ADD("add new item"),
    REMOVE("remove item"),
    ALL("print all item"),
    HELP("print all command"),
    Q("exit");

    String description;

    private FunctionName(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
