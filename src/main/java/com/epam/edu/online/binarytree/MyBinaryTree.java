package com.epam.edu.online.binarytree;

import java.util.ArrayList;
import java.util.List;

public class MyBinaryTree<T extends Comparable<T>> {
    private T val;
    private MyBinaryTree left;
    private MyBinaryTree right;
    private MyBinaryTree parent;
    private List<T> listForPrint = new ArrayList<>();

    public T val() {
        return val;
    }
    public MyBinaryTree left() {
        return left;
    }
    public MyBinaryTree right() {
        return right;
    }
    public MyBinaryTree parent() {
        return parent;
    }
    public MyBinaryTree(T val, MyBinaryTree parent) {
        this.val = val;
        this.parent = parent;
    }
    public void add(T...vals){
        for(T v : vals){
            add(v);
        }
    }
    public void add(T val){
        if(val.compareTo(this.val) < 0){
            if(this.left==null){
                this.left = new MyBinaryTree(val, this);
            }
            else if(this.left != null) {
                this.left.add(val);
            }
        }
        else{
            if(this.right==null){
                this.right = new MyBinaryTree(val, this);
            }
            else if(this.right != null){
                this.right.add(val);
            }
        }
    }
    private MyBinaryTree<T> searchItem(MyBinaryTree<T> tree, T val){
        if(tree == null) return null;
        switch (val.compareTo(tree.val)){
            case 1: {
                return searchItem(tree.right, val);
            }
            case -1: {
                return searchItem(tree.left, val);
            }
            case 0: {
                return tree;
            }
            default: {
                return null;
            }
        }
    }
    public MyBinaryTree<T> search(T val){
        return searchItem(this, val);
    }
    public boolean remove(T val){
        MyBinaryTree<T> tree = search(val);
        if(tree == null){
            return false;
        }
        MyBinaryTree<T> curTree;

        if(tree == this){
            if(tree.right!=null) {
                curTree = tree.right;
            }
            else curTree = tree.left;
            while (curTree.left != null) {
                curTree = curTree.left;
            }
            T temp = curTree.val;
            this.remove(temp);
            tree.val = temp;
            return true;
        }
        //Remove leafs
        if(tree.left==null && tree.right==null && tree.parent != null){
            return removeLeafs(tree);
        }
        //Remove node which have left child but have not right child
        if(tree.left != null && tree.right == null){
            return removeLeftChild(tree);
        }
        //Remove node which have right child but have not left child
        if(tree.left == null && tree.right != null){
            return removeRightChild(tree);
        }
        //Remove node which have child on the left and right
        if(tree.right!=null && tree.left!=null) {
            curTree = tree.right;
            while (curTree.left != null) {
                curTree = curTree.left;
            }
            //if left item are first child
            if(curTree.parent == tree) {
                curTree.left = tree.left;
                tree.left.parent = curTree;
                curTree.parent = tree.parent;
                if (tree == tree.parent.left) {
                    tree.parent.left = curTree;
                } else if (tree == tree.parent.right) {
                    tree.parent.right = curTree;
                }
                return true;
            }
            //if left item are not first child
            else {
                if (curTree.right != null) {
                    curTree.right.parent = curTree.parent;
                }
                curTree.parent.left = curTree.right;
                curTree.right = tree.right;
                curTree.left = tree.left;
                tree.left.parent = curTree;
                tree.right.parent = curTree;
                curTree.parent = tree.parent;
                if (tree == tree.parent.left) {
                    tree.parent.left = curTree;
                } else if (tree == tree.parent.right) {
                    tree.parent.right = curTree;
                }
                return true;
            }
        }
        return false;
    }

    private boolean removeRightChild(MyBinaryTree<T> tree) {
        tree.right.parent = tree.parent;
        if(tree == tree.parent.left){
            tree.parent.left = tree.right;
        }
        else if(tree == tree.parent.right){
            tree.parent.right = tree.right;
        }
        return true;
    }

    private boolean removeLeftChild(MyBinaryTree<T> tree) {
        tree.left.parent = tree.parent;
        if(tree == tree.parent.left){
            tree.parent.left = tree.left;
        }
        else if(tree == tree.parent.right){
            tree.parent.right = tree.left;
        }
        return true;
    }

    private boolean removeLeafs(MyBinaryTree<T> tree) {
        if(tree == tree.parent.left)
            tree.parent.left = null;
        else {
            tree.parent.right = null;
        }
        return true;
    }

    private void print(MyBinaryTree<T> node){
        if(node == null) return;
        print(node.left);
        listForPrint.add(node.val);
        System.out.print(node + " ");
        if(node.right!=null)
            print(node.right);
    }
    public void print(){
        listForPrint.clear();
        System.out.print("List item: ");
        print(this);
        System.out.println();
    }
    @Override
    public String toString() {
        return val.toString();
    }
}