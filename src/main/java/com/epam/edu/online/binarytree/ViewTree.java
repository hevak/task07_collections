package com.epam.edu.online.binarytree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewTree {
    private final static Logger LOGGER = LogManager.getLogger(ViewTree.class);

    public void runTreeDemo(){
        MyBinaryTree<Integer> tree = new MyBinaryTree<>(35, null);
        tree.add(5, 20, 4, 18, 191, 99, 17, 31, 55);

        tree.print();

        tree.remove(256);
        tree.remove(55);
        tree.print();

        LOGGER.info(tree);
        LOGGER.info(tree.left());
        LOGGER.info(tree.left().left());
    }
}
